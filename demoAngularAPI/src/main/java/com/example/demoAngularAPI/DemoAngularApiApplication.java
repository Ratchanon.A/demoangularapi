package com.example.demoAngularAPI;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoAngularApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoAngularApiApplication.class, args);
	}

}
